<?php
// If you want to ignore the uploaded files,
// set $demo_mode to true;
$demo_mode = false;
$upload_dir = 'uploads/';
$allowed_ext = array('jpg','jpeg','png','gif','tiff','bmp');
// ----------------DataBase_Connection------------------------
$databaseHost = "localhost";
$databaseUser = "root";
$databasePassword = "";
$databaseName = "proba";
$connection = mysql_connect($databaseHost, $databaseUser, $databasePassword, $databaseName) or die("Error " . mysql_error($connection));
// ----------------------------------------
if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
    exit_status('Error! Wrong HTTP method!');
}

if(array_key_exists('pic',$_FILES) && $_FILES['pic']['error'] == 0 ){

    $pic = $_FILES['pic'];
    
//-------------------------------------------
    	function get_IP_address()
{
    foreach (array('HTTP_CLIENT_IP',
                   'HTTP_X_FORWARDED_FOR',
                   'HTTP_X_FORWARDED',
                   'HTTP_X_CLUSTER_CLIENT_IP',
                   'HTTP_FORWARDED_FOR',
                   'HTTP_FORWARDED',
                   'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $IPaddress){
                $IPaddress = trim($IPaddress); // Just to be safe

                if (filter_var($IPaddress,
                               FILTER_VALIDATE_IP,
                               FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)
                    !== false) {

                    return $IPaddress;
                }
            }
        }
    }
}
$user_ip = get_IP_address();
file_put_contents('ip.txt', PHP_EOL . $user_ip,FILE_APPEND);
//------------------TEST_THINGS--------------------
$imageData = getimagesize($_FILES['pic']['tmp_name']);
$type = image_type_to_extension($imageData[2]); // Get a image Type
$image_name = uniqid() . $type; // Generate a random name
$url_address = 'http://127.0.0.1/uploads/' . $image_name;
mysql_query("INSERT INTO `proba`.`help` (`id`, `name`, `ip`, `url`) VALUES (NULL, '$image_name', '$user_ip', '$url_address')");
file_put_contents('name.txt', PHP_EOL . $image_name,FILE_APPEND);
//-------------------------------------------

//-----------------Image_Folder-----------------
 $image_folder = 'uploads/' . $image_name;

//----------------------------------------------

    if(!in_array(get_extension($pic['name']),$allowed_ext)){
        exit_status('Only '.implode(',',$allowed_ext).' files are allowed!');
    }	

    if($demo_mode){

        // File uploads are ignored. We only log them.

        $line = implode('		', array( date('r'), $_SERVER['REMOTE_ADDR'], $pic['size'], $pic['name']));
        file_put_contents('log.txt', $line.PHP_EOL, FILE_APPEND);

        exit_status('Uploads are ignored in demo mode.');
    }

    // Move the uploaded file from the temporary
    // directory to the uploads folder:

    if(move_uploaded_file($pic['tmp_name'], $upload_dir.$image_name)){
        exit_status('File was uploaded successfuly!');
    }
}

exit_status('Something went wrong with your upload!');

// Helper functions

function exit_status($str){
    echo json_encode(array('status'=>$str));
    exit;
}

function get_extension($file_name){
    $ext = explode('.', $file_name);
    $ext = array_pop($ext);
    return strtolower($ext);
}

?>